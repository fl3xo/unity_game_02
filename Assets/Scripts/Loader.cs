﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour
{ 
	public GameManager GameManager;

	void Awake ()
	{
		if (GameManager.Instance == null) {
			Instantiate (GameManager);
		}
		GameManager.Instance.SetGameState (GameState.GAME);
	}
}
