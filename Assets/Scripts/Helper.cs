﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PathNode
{
	public int X;
	public int Y;
	public bool IsBlocking;

	public int G;
	public int H;
	public int F { get { return G + H; } }

	public List<PathNode> ChildNodes = new List<PathNode> ();

	public PathNode (Vector2 v)
	{
		X = (int)v.x;
		Y = (int)v.y;
	}

	public override string ToString ()
	{
		return string.Format ("PathNode - {0}, {1}", X, Y);
	}

}

public static class Helper
{
	public static uint GetUInt (int i)
	{
		return Convert.ToUInt32 (i > 0 ? i : i * -1);
	}
}