﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
	[Serializable]
	public class Count
	{
		public int Minimum;
		public int Maximum;

		public Count (int min, int max)
		{
			Minimum = min;
			Maximum = max;
		}
	}

	public int Columns = 8;
	public int Rows = 8;
	public Count WallCount = new Count (5, 9);
	public Count FoodCount = new Count (1, 5);

	public int BlockingLayerId = 8;

	public GameObject Exit;
	public GameObject[] FloorTiles;
	public GameObject[] WallTiles;
	public GameObject[] FoodTiles;
	public GameObject[] EnemyTiles;
	public GameObject[] OuterWallTiles;

	private Transform _boardHolder;
	private List<Vector3> _gridPositions = new List<Vector3> ();

	void InitializeList ()
	{
		_gridPositions.Clear ();
		for (int x=1; x<Columns-1; x++) {
			for (int y=1; y<Rows-1; y++) {
				var v3 = new Vector3 (x, y, 0f);
				_gridPositions.Add (v3);
			}
		}
	}

	void BoardSetup ()
	{
		_boardHolder = new GameObject ("Board").transform;
		for (int x=-1; x<Columns+1; x++) {
			for (int y=-1; y<Rows+1; y++) {
				GameObject toInstantiate = FloorTiles [Random.Range (0, FloorTiles.Length)];
				if (x == -1 || x == Columns || y == -1 || y == Rows)
					toInstantiate = OuterWallTiles [Random.Range (0, OuterWallTiles.Length)];
				
				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (_boardHolder);
			}
		}
	}
	
	Vector3 RandomPosition ()
	{
		int randomIndex = Random.Range (0, _gridPositions.Count);
		Vector3 randomPosition = _gridPositions [randomIndex];
		_gridPositions.RemoveAt (randomIndex);
		return randomPosition;
	}
	
	public void CreateWallTile (float x, float y)
	{
		CreateWallTile ((int)x, (int)y);
	}
	
	public void CreateWallTile (int x, int y)
	{
		if (CheckBlock (x, y)) {
			for (var i=0; i<_boardHolder.childCount; i++) {
				var child = _boardHolder.GetChild (i);
				if (child.position.x == x && child.position.y == y) {
					if (child.gameObject.layer == BlockingLayerId) {
						Destroy (child.gameObject);
						return;
					}
				}
			}
			return;
		} else {
			InstantiateWithParent (WallTiles [0], new Vector3 (x, y, 0f), Quaternion.identity, _boardHolder);
		}
	}
	
	void LayoutObjectAtRandom (GameObject[] tileArray, int minimum, int maximum)
	{
		int objectCount = Random.Range (minimum, maximum + 1);
		for (var i=0; i<objectCount; i++) {
			var randomPosition = RandomPosition ();
			GameObject tileChoice = tileArray [Random.Range (0, tileArray.Length)];
			Instantiate (tileChoice, randomPosition, Quaternion.identity);
		}
	}
	
	public void InstantiateWithParent (GameObject toInstantiate, Vector3 position, Quaternion rotation, Transform parent = null)
	{
		GameObject instance = Instantiate (toInstantiate, position, rotation) as GameObject;
		if (parent != null) {
			instance.transform.SetParent (parent);
		}
	}
	
	public void SetupScene (int level)
	{
		BoardSetup ();
		InitializeList ();

//		CreateWallTile (3, 3);
//		CreateWallTile (3, 4);
//		CreateWallTile (3, 5);
//		CreateWallTile (3, 6);

//		InstantiateWithParent (WallTiles [0], new Vector3 (3f, 3f, 0f), Quaternion.identity, _boardHolder);
//		InstantiateWithParent (WallTiles [0], new Vector3 (3f, 4f, 0f), Quaternion.identity, _boardHolder);
//		InstantiateWithParent (WallTiles [0], new Vector3 (3f, 5f, 0f), Quaternion.identity, _boardHolder);
//		InstantiateWithParent (WallTiles [0], new Vector3 (3f, 6f, 0f), Quaternion.identity, _boardHolder);
//		Instantiate (WallTiles [0], new Vector3 (2f, 4f, 0f), Quaternion.identity);
		//Instantiate (WallTiles [0], new Vector3 (2f, 5f, 0f), Quaternion.identity);
		//Instantiate (WallTiles [0], new Vector3 (3f, 5f, 0f), Quaternion.identity);
		
		LayoutObjectAtRandom (WallTiles, WallCount.Minimum, WallCount.Maximum);
		LayoutObjectAtRandom (FoodTiles, FoodCount.Minimum, FoodCount.Maximum);
		int enemyCount = (int)Mathf.Log (level, 2);
		LayoutObjectAtRandom (EnemyTiles, enemyCount, enemyCount);
		InstantiateWithParent (Exit, new Vector3 (Columns - 1, Rows - 1, 0f), Quaternion.identity, _boardHolder);
	}


	public void ResetFloorTiles ()
	{
		for (var i=0; i<_boardHolder.childCount; i++) {
			var child = _boardHolder.GetChild (i);
			var spriteRenderer = child.GetComponent<SpriteRenderer> ();
			if (spriteRenderer != null) {
				spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 1f);
			}
		}
	}
	
	public void HighlightFloorTile (float x, float y)
	{
		HighlightFloorTile ((int)x, (int)y);
	}
	
	public void HighlightFloorTile (int x, int y)
	{
		for (var i=0; i<_boardHolder.childCount; i++) {
			var child = _boardHolder.GetChild (i);
			if (child.position.x == x && child.position.y == y) {
				var spriteRenderer = child.GetComponent<SpriteRenderer> ();
				if (spriteRenderer != null) {
					spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 0.5f);
				}
			}
		}
	}
	
	public bool CheckBlock (float x, float y)
	{
		var result = false;
		for (var i=0; i<_boardHolder.childCount; i++) {
			var child = _boardHolder.GetChild (i);
			if (child.position.x == x && child.position.y == y) {
				var spriteRenderer = child.GetComponent<SpriteRenderer> ();
				var gameObject = spriteRenderer.gameObject;
				result = gameObject.layer == BlockingLayerId;
			}
		}
		return result;
	}



}
