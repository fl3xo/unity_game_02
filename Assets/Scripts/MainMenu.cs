﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
	public Button startGameButton;
	public Button exitGameButton;
	public Button scoreButton;

	public GameManager GameManager;
	
	void Awake ()
	{ 
		if (GameManager.Instance == null) {
			Instantiate (GameManager);
		}

		startGameButton.onClick.AddListener (() => OnStartGameClick ());
		exitGameButton.onClick.AddListener (() => OnExitGameClick ());
		scoreButton.onClick.AddListener (() => OnScoreClick ());
	}

	void Start ()
	{
		GameManager.Instance.SetGameState (GameState.INTRO);
	}

	void OnStartGameClick ()
	{
		if (GameManager.Instance.FirstStart) {
			GameManager.Instance.FirstStart = false;
			GameManager.Instance.SetGameState (GameState.GAME);
			GameManager.Instance.Level = 0;
			Application.LoadLevel ("Game");
		} else {
			GameManager.Instance.RestartGame ();
		}
	}

	void OnScoreClick ()
	{
		GameManager.Instance.SetGameState (GameState.HIGHSCORE);
		Application.LoadLevel ("Score");
	}

	void OnExitGameClick ()
	{
		Application.Quit ();
	}

	// Update is called once per frame
	void Update ()
	{
	
	}
}
