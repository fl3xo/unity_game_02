﻿using UnityEngine;
using System.Collections;

public class GridOverlay : MonoBehaviour
{
	#region GridOverlay
	
	public bool showMain = true;
	public bool showSub = false;
	
	public int gridSizeX;
	public int gridSizeY;
	
	public float smallStep;
	public float largeStep;
	
	public float startX;
	public float startY;
	
	public float offsetY = 0;
	public float offsetX = 0;
	private float scrollRate = 0.1f;
	private float lastScroll = 0f;
	
	private Material lineMaterial;
	
	private Color mainColor = new Color (0f, 1f, 0f, 1f);
	private Color subColor = new Color (0f, 0.5f, 0f, 1f);
	
	#endregion

	void CreateLineMaterial ()
	{
		
		if (!lineMaterial) {
			lineMaterial = new Material ("Shader \"Lines/Colored Blended\" {" +
				"SubShader { Pass { " +
				"    Blend SrcAlpha OneMinusSrcAlpha " +
				"    ZWrite Off Cull Off Fog { Mode Off } " +
				"    BindChannels {" +
				"      Bind \"vertex\", vertex Bind \"color\", color }" +
				"} } }");
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		}
	}

	void OnGUI ()
	{
//		var guiStyle = new GUIStyle (GUI.skin.GetStyle ("Label"));
//		guiStyle.fontSize = 10;
//		guiStyle.normal.textColor = new Color (255, 255, 255);
//
//		var i = 0;
//		for (var x = 0; x <= 7; x++) {
//			var j = 0;
//			for (var y = 7; y >= 0; y--) {
//				var topLeft = Camera.main.WorldToScreenPoint (new Vector3 (x, y, 0f));
//				var bottomRight = Camera.main.WorldToScreenPoint (new Vector3 (x, y, 0f));
//				GUI.Label (new Rect (topLeft.x, topLeft.y, bottomRight.x, bottomRight.y), string.Format ("{0},{1}", i, j), guiStyle);
//				j++;
//			}
//			i++;
//		}
	}

	void OnPostRender ()
	{        
//		CreateLineMaterial ();
//		lineMaterial.SetPass (0);
//		
//		GL.Begin (GL.LINES);
//		
//		if (showSub) {
//			GL.Color (subColor);
//
//			for (float j = 0; j <= gridSizeY; j += smallStep) {
//				for (float i = 0; i <= gridSizeX; i += smallStep) {
//					GL.Vertex3 (startX + offsetX, j + offsetY, 0);
//					GL.Vertex3 (gridSizeX + offsetX, j + offsetY, 0);
//				}
//			}
//
//			for (float j = 0; j <= gridSizeX; j += smallStep) {
//				for (float i = 0; i <= gridSizeY; i += smallStep) {
//					GL.Vertex3 (j + offsetX, startY + offsetY, 0);
//					GL.Vertex3 (j + offsetX, gridSizeY + offsetY, 0);
//				}
//			}
//		}
//		
//		if (showMain) {
//			GL.Color (mainColor);
//			
//			for (float j = 0; j <= gridSizeY; j += largeStep) {
//				for (float i = 0; i <= gridSizeX; i += largeStep) {
//
//					GL.Vertex3 (startX + offsetX, j + offsetY, 0);
//					GL.Vertex3 (gridSizeX + offsetX, j + offsetY, 0);
//				}
//			}
//			
//			for (float j = 0; j <= gridSizeX; j += largeStep) {
//				for (float i = 0; i <= gridSizeY; i += largeStep) {
//					GL.Vertex3 (j + offsetX, startY + offsetY, 0);
//					GL.Vertex3 (j + offsetX, gridSizeY + offsetY, 0);
//				}
//			}
//		}
//		
//		GL.End ();
	}
}
