﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//Player inherits from MovingObject, our base class for objects that can move, Enemy also inherits from this.
public class Player : MovingObject
{
	public float restartLevelDelay = 1f;		//Delay time in seconds to restart level.
	public int pointsPerFood = 10;				//Number of points to add to player food points when picking up a food object.
	public int pointsPerSoda = 20;				//Number of points to add to player food points when picking up a soda object.
	public int wallDamage = 1;					//How much damage a player does to a wall when chopping it.
	public Text foodText;						//UI Text to display current player food total.
	public AudioClip moveSound1;				//1 of 2 Audio clips to play when player moves.
	public AudioClip moveSound2;				//2 of 2 Audio clips to play when player moves.
	public AudioClip eatSound1;					//1 of 2 Audio clips to play when player collects a food object.
	public AudioClip eatSound2;					//2 of 2 Audio clips to play when player collects a food object.
	public AudioClip drinkSound1;				//1 of 2 Audio clips to play when player collects a soda object.
	public AudioClip drinkSound2;				//2 of 2 Audio clips to play when player collects a soda object.
	public AudioClip gameOverSound;				//Audio clip to play when player dies.
	
	private Animator animator;					//Used to store a reference to the Player's animator component.
	private int food;							//Used to store player food points total during level.
	private Vector2 touchOrigin = -Vector2.one;	//Used to store location of screen touch origin for mobile controls.
	
	
	//Start overrides the Start function of MovingObject
	protected override void Start ()
	{
		//Get a component reference to the Player's animator component
		animator = GetComponent<Animator> ();
	
		InitPlayer ();

		//Set the foodText to reflect the current player food total.
		foodText.text = "Food: " + food;
		
		//Call the Start function of the MovingObject base class.
		base.Start ();
	}

	public void InitPlayer ()
	{
		//Get the current food point total stored in GameManager.instance between levels.
		food = GameManager.Instance.PlayerFoodPoints;
	}
	
	
	//This function is called when the behaviour becomes disabled or inactive.
	private void OnDisable ()
	{
		//When Player object is disabled, store the current local food total in the GameManager so it can be re-loaded in next level.
		GameManager.Instance.PlayerFoodPoints = food;
	}
	
	
	private void Update ()
	{
		Debug.Log (GameManager.Instance.PlayersTurn);
		//If it's not the player's turn, exit the function.
		if (!GameManager.Instance.PlayersTurn)
			return;

		int horizontal = 0;  	//Used to store the horizontal move direction.
		int vertical = 0;		//Used to store the vertical move direction.
		
		//Check if we are running either in the Unity editor or in a standalone build.
		#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
		
		//Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
		horizontal = (int)(Input.GetAxisRaw ("Horizontal"));
		
		//Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
		vertical = (int)(Input.GetAxisRaw ("Vertical"));
		
		//Check if moving horizontally, if so set vertical to zero.
		if (horizontal != 0) {
			vertical = 0;
		}
		//Check if we are running on iOS, Android, Windows Phone 8 or Unity iPhone
		#elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
		
		//Check if Input has registered more than zero touches
		if (Input.touchCount > 0)
		{
			//Store the first touch detected.
			Touch myTouch = Input.touches[0];
			
			//Check if the phase of that touch equals Began
			if (myTouch.phase == TouchPhase.Began)
			{
				//If so, set touchOrigin to the position of that touch
				touchOrigin = myTouch.position;
			}
			
			//If the touch phase is not Began, and instead is equal to Ended and the x of touchOrigin is greater or equal to zero:
			else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
			{
				//Set touchEnd to equal the position of this touch
				Vector2 touchEnd = myTouch.position;
				
				//Calculate the difference between the beginning and end of the touch on the x axis.
				float x = touchEnd.x - touchOrigin.x;
				
				//Calculate the difference between the beginning and end of the touch on the y axis.
				float y = touchEnd.y - touchOrigin.y;
				
				//Set touchOrigin.x to -1 so that our else if statement will evaluate false and not repeat immediately.
				touchOrigin.x = -1;
				
				//Check if the difference along the x axis is greater than the difference along the y axis.
				if (Mathf.Abs(x) > Mathf.Abs(y))
					//If x is greater than zero, set horizontal to 1, otherwise set it to -1
					horizontal = x > 0 ? 1 : -1;
				else
					//If y is greater than zero, set horizontal to 1, otherwise set it to -1
					vertical = y > 0 ? 1 : -1;
			}
		}
		
		#endif //End of mobile platform dependendent compilation section started above with #elif
		//Check if we have a non-zero value for horizontal or vertical
		if (horizontal != 0 || vertical != 0) {
			//Call AttemptMove passing in the generic parameter Wall, since that is what Player may interact with if they encounter one (by attacking it)
			//Pass in horizontal and vertical as parameters to specify the direction to move Player in.
			AttemptMove<Wall> (horizontal, vertical);
		}
	}
	
	//AttemptMove overrides the AttemptMove function in the base class MovingObject
	//AttemptMove takes a generic parameter T which for Player will be of the type Wall, it also takes integers for x and y direction to move in.
	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		//Every time player moves, subtract from food points total.
		food--;
		
		//Update food text display to reflect current score.
		foodText.text = "Food: " + food;
		
		//Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
		base.AttemptMove <T> (xDir, yDir);
		
		//Hit allows us to reference the result of the Linecast done in Move.
		RaycastHit2D hit;
		
		//If Move returns true, meaning Player was able to move into an empty space.
		if (Move (xDir, yDir, out hit)) {
			//Call RandomizeSfx of SoundManager to play the move sound, passing in two audio clips to choose from.
			SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
		}
		
		//Since the player has moved and lost food points, check if the game has ended.
		CheckIfGameOver ();
		
		//Set the playersTurn boolean of GameManager to false now that players turn is over.
		GameManager.Instance.PlayersTurn = false;
	}
	
	
	//OnCantMove overrides the abstract function OnCantMove in MovingObject.
	//It takes a generic parameter T which in the case of Player is a Wall which the player can attack and destroy.
	protected override void OnCantMove <T> (T component)
	{
		//Set hitWall to equal the component passed in as a parameter.
		Wall hitWall = component as Wall;
		
		//Call the DamageWall function of the Wall we are hitting.
		hitWall.DamageWall (wallDamage);
		
		//Set the attack trigger of the player's animation controller in order to play the player's attack animation.
		animator.SetTrigger ("playerChop");
	}
	
	
	//OnTriggerEnter2D is sent when another object enters a trigger collider attached to this object (2D physics only).
	private void OnTriggerEnter2D (Collider2D other)
	{
		//Check if the tag of the trigger collided with is Exit.
		if (other.tag == "Exit") {
			//Invoke the Restart function to start the next level with a delay of restartLevelDelay (default 1 second).
			Invoke ("Restart", restartLevelDelay);
			
			//Disable the player object since level is over.
			enabled = false;
		}
		
		//Check if the tag of the trigger collided with is Food.
		else if (other.tag == "Food") {
			//Add pointsPerFood to the players current food total.
			food += pointsPerFood;
			
			//Update foodText to represent current total and notify player that they gained points
			foodText.text = "+" + pointsPerFood + " Food: " + food;
			
			//Call the RandomizeSfx function of SoundManager and pass in two eating sounds to choose between to play the eating sound effect.
			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);
			
			//Disable the food object the player collided with.
			other.gameObject.SetActive (false);
		}
		
		//Check if the tag of the trigger collided with is Soda.
		else if (other.tag == "Soda") {
			//Add pointsPerSoda to players food points total
			food += pointsPerSoda;
			
			//Update foodText to represent current total and notify player that they gained points
			foodText.text = "+" + pointsPerSoda + " Food: " + food;
			
			//Call the RandomizeSfx function of SoundManager and pass in two drinking sounds to choose between to play the drinking sound effect.
			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);
			
			//Disable the soda object the player collided with.
			other.gameObject.SetActive (false);
		}
	}
	
	
	//Restart reloads the scene when called.
	private void Restart ()
	{
		//Load the last scene loaded, in this case Main, the only scene in the game.
		Application.LoadLevel (Application.loadedLevel);
	}
	
	
	//LoseFood is called when an enemy attacks the player.
	//It takes a parameter loss which specifies how many points to lose.
	public void LoseFood (int loss)
	{
		//Set the trigger for the player animator to transition to the playerHit animation.
		animator.SetTrigger ("playerHit");
		
		//Subtract lost food points from the players total.
		food -= loss;
		
		//Update the food display with the new total.
		foodText.text = "-" + loss + " Food: " + food;
		
		//Check to see if game has ended.
		CheckIfGameOver ();
	}
	
	
	//CheckIfGameOver checks if the player is out of food points and if so, ends the game.
	private void CheckIfGameOver ()
	{
		//Check if food point total is less than or equal to zero.
		if (food <= 0) {
			//Call the PlaySingle function of SoundManager and pass it the gameOverSound as the audio clip to play.
			SoundManager.instance.PlaySingle (gameOverSound);
			
			//Stop the background music.
			SoundManager.instance.musicSource.Stop ();
			
			//Call the GameOver function of GameManager.
			GameManager.Instance.GameOver ();
		}
	}
}

//public class Player : MovingObject
//{
//	public Text FoodText;
//	public int WallDamage = 1;
//	public int PointsPerFood = 10;
//	public int PointsPerSoda = 20;
//
//	public AudioClip moveSound1;
//	public AudioClip moveSound2;
//	public AudioClip eatSound1;
//	public AudioClip eatSound2;
//	public AudioClip drinkSound1;
//	public AudioClip drinkSound2;
//	public AudioClip gameOverSound;
//
//	public float RestartLevelDelay = 1f;
//
//	private Animator _animator;
//	private int _food;
//
//	private Vector2 touchOrigin = -Vector2.one;
//
//	// Use this for initialization
//	protected override void Start ()
//	{
//		_animator = GetComponent<Animator> ();
//		_food = GameManager.Instance.PlayerFoodPoints;
//		FoodText.text = "Food: " + _food;
//		base.Start ();
//	}
//
//	private void OnDisable ()
//	{
//		if (GameManager.Instance != null && GameManager.Instance.PlayerFoodPoints != null) {
//			GameManager.Instance.PlayerFoodPoints = _food;
//		}
//	}
//
//	private void CheckIfGameOver ()
//	{
//		if (_food <= 0) {
//			SoundManager.instance.PlaySingle (gameOverSound);
//			SoundManager.instance.musicSource.Stop ();
//			
//			GameManager.Instance.GameOver ();
//		}
//	}
//
//	protected override void AttemptMove<T> (int xDir, int yDir)
//	{
//		_food --;
//
//		FoodText.text = "Food: " + _food;
//
//		base.AttemptMove<T> (xDir, yDir);
//
//		RaycastHit2D hit;
//		if (Move (xDir, yDir, out hit)) {
//			//Think (xDir, yDir);
//			SoundManager.instance.RandomizeSfx (moveSound1, moveSound2);
//		}
//
//		CheckIfGameOver ();
//
//		GameManager.Instance.PlayersTurn = false;
//	}
//
//	private List<PathNode> checkedNodes = new List<PathNode> ();
//
//	void CreateChildNodes (PathNode startingNode, PathNode exitNode)
//	{
//		var x = 17;
//
//		if (startingNode.X == 3 && startingNode.Y == 2) {
//			x += 5;
//			x *= 4;
//			x = 1;
//			Debug.Log ("AAAAAARGH...");
//		}
//
//		if (startingNode.X == exitNode.X && startingNode.Y == exitNode.Y)
//			return;
//		var topChild = new PathNode (new Vector2 (startingNode.X, startingNode.Y + 1));
//		var bottomChild = new PathNode (new Vector2 (startingNode.X, startingNode.Y - 1));
//		var leftChild = new PathNode (new Vector2 (startingNode.X - 1, startingNode.Y));
//		var rightChild = new PathNode (new Vector2 (startingNode.X + 1, startingNode.Y));
//
//		startingNode.ChildNodes.Add (topChild);
//		startingNode.ChildNodes.Add (bottomChild);
//		startingNode.ChildNodes.Add (leftChild);
//		startingNode.ChildNodes.Add (rightChild);
//
//		foreach (var node in startingNode.ChildNodes.ToList ()) {
//			if (node.X == 4 && node.Y == 2) {
//				x += 5;
//				x *= 4;
//				x = 1;
//				Debug.Log ("AAAAAARGH...");
//			}
//			var checkedNode = checkedNodes.SingleOrDefault (a => a.X == node.X && a.Y == node.Y);
//			if (checkedNode == null) {
//				var isBlock = GameManager.Instance.BoardScript.CheckBlock (node.X, node.Y);
//				node.IsBlocking = isBlock;
//				node.G = 10;
//				node.H = ((exitNode.X - node.X) + (exitNode.Y - node.Y)) * 10;
//				checkedNodes.Add (node);
//				if (!node.IsBlocking) {
//					CreateChildNodes (node, exitNode);
//				} else {
//					startingNode.ChildNodes.Remove (node);
//				}
//			} else {
//				node.IsBlocking = checkedNode.IsBlocking;
//				node.ChildNodes = checkedNode.ChildNodes.ToList ();
//				node.G = checkedNode.G;
//				node.H = checkedNode.H;
//			}
//		}
//	}
//
//	void Think (int x, int y)
//	{
//		GameManager.Instance.BoardScript.ResetFloorTiles ();
//		checkedNodes.Clear ();
//		var exitPosition = new Vector2 (GameManager.Instance.BoardScript.Columns - 1, GameManager.Instance.BoardScript.Rows - 1);
//		var targetPositions = new List<PathNode> ();
//		var simulatedPosition = new Vector2 ((int)transform.position.x, (int)transform.position.y);
//
//		var startingNode = new PathNode (simulatedPosition);
//		var exitNode = new PathNode (exitPosition);
//		checkedNodes.Add (startingNode);
//		CreateChildNodes (startingNode, exitNode);
//		targetPositions.Add (startingNode);
//
//		var currentNode = startingNode;
//		while (currentNode.X != exitNode.X || currentNode.Y != exitNode.Y) {
//			var lowestF = currentNode.ChildNodes.Where (a => a.F > 0 && !a.IsBlocking).Min (a => a.F);
//			var childNode = currentNode.ChildNodes.First (a => a.F == lowestF);
//			currentNode = childNode;
//			targetPositions.Add (currentNode);
//
//			if (targetPositions.Count > 500)
//				break;
//		}
//
//		targetPositions.ForEach (a => GameManager.Instance.BoardScript.HighlightFloorTile (a.X, a.Y));
//		return;
//
//		// woking, but not smart...
//		targetPositions.Add (new PathNode (exitPosition));
//				
//		simulatedPosition.x += x;
//		simulatedPosition.y += y;
//		
//		var count = 0;
//		while (simulatedPosition.x != exitPosition.x || simulatedPosition.y != exitPosition.y) {
//			int diffX = (int)(simulatedPosition.x - exitPosition.x);
//			int diffY = (int)(simulatedPosition.y - exitPosition.y);
//			
//			uint uX = Helper.GetUInt (diffX);
//			uint uY = Helper.GetUInt (diffY);
//			
//			if (uX > uY) {
//				var nextX = simulatedPosition.x + (diffX > 0 ? -1 : 1);
//				var isBlock = GameManager.Instance.BoardScript.CheckBlock (nextX, simulatedPosition.y);
//				if (!isBlock) {
//					simulatedPosition.x = nextX;
//				}
//				
//				while (isBlock) {
//					var nextY = simulatedPosition.y + (diffY > 0 ? -1 : 1);
//					simulatedPosition.y = nextY;
//
//					break;
//				}
//			} else {
//				var nextY = simulatedPosition.y + (diffY > 0 ? -1 : 1);
//				var isBlock = GameManager.Instance.BoardScript.CheckBlock (simulatedPosition.x, nextY);
//				if (!isBlock) {
//					simulatedPosition.y = nextY;
//				}
//
//				while (isBlock) {
//					var nextX = simulatedPosition.x + (diffX > 0 ? -1 : 1);
//					simulatedPosition.x = nextX;
//					break;
//				}
//			}
//			
//			targetPositions.Add (new PathNode (simulatedPosition));
//			
//			count++;
//			if (count == 100)
//				break;
//		}
//		
//		targetPositions.ForEach (a => GameManager.Instance.BoardScript.HighlightFloorTile (a.X, a.Y));
//	}
//
//	// Update is called once per frame
//	void Update ()
//	{
////		if (Input.GetMouseButtonDown (0)) {
////			var targetPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
////			GameManager.Instance.BoardScript.CreateWallTile (targetPosition.x, targetPosition.y);
////			Think (0, 0);
////		}
//
//		if (!GameManager.Instance.PlayersTurn)
//			return;
//
//		int horizontal = 0;
//		int vertical = 0;
//		
//#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
//
//		horizontal = (int)Input.GetAxisRaw ("Horizontal");
//		vertical = (int)Input.GetAxisRaw ("Vertical");
//		if (horizontal != 0)
//			vertical = 0;
//#elif UNITY_METRO || UNITY_WP_8_1 || UNITY_WINRT || UNITY_WINRT_8_1
//		if (Input.touchCount > 0) {
//			Touch myTouch = Input.touches [0];
//			if (myTouch.phase == TouchPhase.Began) {
//				touchOrigin = myTouch.position;
//			} else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0) {
//				Vector2 touchEnd = myTouch.position;
//				
//				float x = touchEnd.x - touchOrigin.x;
//				float y = touchEnd.y - touchOrigin.y;
//				touchOrigin.x = -1;
//				
//				if (Mathf.Abs (x) > Mathf.Abs (y))
//					horizontal = x > 0 ? 1 : -1;
//				else
//					vertical = y > 0 ? 1 : -1;
//			}
//		}
//#else 
//		if (Input.touchCount > 0) {
//			Touch myTouch = Input.touches [0];
//			if (myTouch.phase == TouchPhase.Began) {
//				touchOrigin = myTouch.position;
//			} else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0) {
//				Vector2 touchEnd = myTouch.position;
//
//				float x = touchEnd.x - touchOrigin.x;
//				float y = touchEnd.y - touchOrigin.y;
//				touchOrigin.x = -1;
//
//				if (Mathf.Abs (x) > Mathf.Abs (y))
//					horizontal = x > 0 ? 1 : -1;
//				else
//					vertical = y > 0 ? 1 : -1;
//			}
//		}
//#endif
//
//		if (horizontal != 0 || vertical != 0) {
//			AttemptMove<Wall> (horizontal, vertical);
//		}
//	}
//
//	protected override void OnCantMove<T> (T component)
//	{
//		Wall hitWall = component as Wall;
//		hitWall.DamageWall (WallDamage);
//
//		_animator.SetTrigger ("playerChop");
//	}
//
//	void OnTriggerEnter2D (Collider2D other)
//	{
//		if (other.tag == "Exit") {
//			Invoke ("Restart", RestartLevelDelay);
//			enabled = false;
//		} else if (other.tag == "Food") {
//			_food += PointsPerFood;
//			FoodText.text = "+" + PointsPerFood + " Food: " + _food;
//			SoundManager.instance.RandomizeSfx (eatSound1, eatSound2);
//			other.gameObject.SetActive (false);
//		} else if (other.tag == "Soda") {
//			_food += PointsPerSoda;
//			FoodText.text = "+" + PointsPerSoda + " Food: " + _food;
//			SoundManager.instance.RandomizeSfx (drinkSound1, drinkSound2);
//			other.gameObject.SetActive (false);
//		}
//	}
//
//	void Restart ()
//	{
//		Application.LoadLevel (Application.loadedLevel);
//	}
//
//	public void LooseFood (int loss)
//	{
//		_animator.SetTrigger ("playerHit");
//		_food -= loss;
//
//		FoodText.text = "-" + loss + " Food: " + _food;
//
//		CheckIfGameOver ();
//	}
//
//
//}
