﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
	public Button backButton;
	public Button resetButton;
	public GameObject highscoreList;
	
	public GameManager GameManager;
	
	void Awake ()
	{ 
		if (GameManager.Instance == null) {
			Instantiate (GameManager);
		}
		backButton.onClick.AddListener (() => OnBackClick ());
		resetButton.onClick.AddListener (() => OnResetClick ());
		LoadScores ();
	}
	
	void LoadScores ()
	{
		string scoreKey = "HScore";
		string scoreNameKey = "HScoreName";		       
		string m_leaderBoardText = "\n \t Local Top 10: \n\n";			
		       
		var textElements = highscoreList.GetComponentsInChildren<Text> ();

		for (int i=0; i<10; i++) {
			var daysSurvived = PlayerPrefs.GetInt (scoreKey + i);
			var rank = i + 1;
			if (daysSurvived != 0) {
				textElements [i].text = string.Format ("{0}. {1} day{2} - {3}", rank.ToString ("D2"), daysSurvived, daysSurvived > 1 ? "s" : "", PlayerPrefs.GetString (scoreNameKey + i));
			} else {
				textElements [i].text = string.Format ("{0}.", rank.ToString ("D2"));
			}
			m_leaderBoardText += "\t" + (i + 1) + ". " + PlayerPrefs.GetInt (scoreKey + i) + " - ";
			m_leaderBoardText += " " + PlayerPrefs.GetString (scoreNameKey + i) + " \n";
		}  
	 
		Debug.Log (m_leaderBoardText); // do what ever you want with the string
	}
	
	void OnBackClick ()
	{
		GameManager.Instance.SetGameState (GameState.MAIN_MENU);
		Application.LoadLevel ("MainMenu");
	}

	void OnResetClick ()
	{
		PlayerPrefs.DeleteAll ();
		GameManager.Instance.SetGameState (GameState.HIGHSCORE);
		Application.LoadLevel (Application.loadedLevel);
	}
}
