﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public enum GameState
{
	INTRO,
	MAIN_MENU,
	GAME,
	HIGHSCORE,
}

public delegate void OnStateChangeHandler ();

public class GameManager : MonoBehaviour
{
	public static GameManager Instance = null;
	public event OnStateChangeHandler OnStateChange;
	public GameState gameState = GameState.INTRO;

	public BoardManager BoardScript;
	public bool FirstStart = true;
	public int Level = 1;
	public float LevelStartDelay = 2f;
	public float TurnDelay = 0.1f;

	public int PlayerFoodPoints = 100;
	[HideInInspector]
	public bool
		PlayersTurn = true;

	private List<Enemy> _enemies;
	private bool _enemiesMoving;
	private bool _doingSetup;
	private Text _levelText;
	private GameObject _levelImage;

	// Use this for initialization
	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);
		_enemies = new List<Enemy> ();
		BoardScript = GetComponent<BoardManager> ();
		InitGame ();
	}

	void OnLevelWasLoaded (int index)
	{
		Level++;
		InitGame ();
	}

	void Update ()
	{
		if (PlayersTurn || _enemiesMoving || _doingSetup)
			return;

		StartCoroutine (MoveEnemies ());
	}

	public void AddEnemyToList (Enemy script)
	{
		_enemies.Add (script);
	}

	void InitGame ()
	{
		if (gameState != GameState.GAME)
			return;

		_doingSetup = true;
		_levelImage = GameObject.Find ("LevelImage");
		var levelTextObject = GameObject.Find ("LevelText");
		if (levelTextObject != null)
			_levelText = levelTextObject.GetComponent<Text> ();
		if (_levelText != null)
			_levelText.text = "Day " + Level;
		if (_levelImage != null)
			_levelImage.SetActive (true);
		Invoke ("HideLevelImage", LevelStartDelay);
		_enemies.Clear ();
		BoardScript.SetupScene (Level);
	}

	void HideLevelImage ()
	{
		if (_levelImage != null)
			_levelImage.SetActive (false);
		_doingSetup = false;
	}

 
	public void AddLocalScore (int daysSurvived, string playerName)
	{
		string scoreKey = "HScore";
		string scoreNameKey = "HScoreName";
	 	       
		int newScore = daysSurvived;
		string newName = System.DateTime.Now.ToString ();
		int oldScore;
		string oldName;
       
		for (int i=0; i<10; i++) {
			if (PlayerPrefs.HasKey (scoreKey + i)) {
				if (PlayerPrefs.GetInt (scoreKey + i) < newScore) {
					// new Score is higher than the stored score
					oldScore = PlayerPrefs.GetInt (scoreKey + i);
					oldName = PlayerPrefs.GetString (scoreNameKey + i);
					
					PlayerPrefs.SetInt (scoreKey + i, newScore);
					PlayerPrefs.SetString (scoreNameKey + i, newName);
					newScore = oldScore;
					newName = oldName; 
				}
			} else {
				PlayerPrefs.SetInt (scoreKey + i, newScore);
				PlayerPrefs.SetString (scoreNameKey + i, newName);
				newScore = 0;
				newName = "";
			}
		}
	} 

	public void GameOver ()
	{
		_levelText.text = "After " + Level + " days, you starved.";
		_levelImage.SetActive (true);
		if (enabled) {
			AddLocalScore (Level, "phreak");
			enabled = false;
			Invoke ("MainMenu", 3f);
		}
	}

	void MainMenu ()
	{
		GameManager.Instance.SetGameState (GameState.MAIN_MENU);
		Application.LoadLevel ("MainMenu");
	}

	//Coroutine to move enemies in sequence.
	IEnumerator MoveEnemies ()
	{
		//While enemiesMoving is true player is unable to move.
		_enemiesMoving = true;
		
		//Wait for turnDelay seconds, defaults to .1 (100 ms).
		yield return new WaitForSeconds (TurnDelay);
		
		//If there are no enemies spawned (IE in first level):
		if (_enemies.Count == 0) {
			//Wait for turnDelay seconds between moves, replaces delay caused by enemies moving when there are none.
			yield return new WaitForSeconds (TurnDelay);
		}
		
		//Loop through List of Enemy objects.
		for (int i = 0; i < _enemies.Count; i++) {
			//Call the MoveEnemy function of Enemy at index i in the enemies List.
			_enemies [i].MoveEnemy ();
			
			//Wait for Enemy's moveTime before moving next Enemy, 
			yield return new WaitForSeconds (_enemies [i].MoveTime);
		}
		//Once Enemies are done moving, set playersTurn to true so player can move.
		PlayersTurn = true;
		
		//Enemies are done moving, set enemiesMoving to false.
		_enemiesMoving = false;
	}

	public void SetActive ()
	{
		gameObject.SetActive (true);
	}

	public void SetGameState (GameState state)
	{
		gameState = state;
		var handler = OnStateChange;
		if (handler != null)
			handler ();
	}

	public void RestartGame ()
	{
		enabled = true;
		GameManager.Instance.SetGameState (GameState.GAME);
		GameManager.Instance.PlayersTurn = true;
		GameManager.Instance.Level = 0;
		GameManager.Instance.PlayerFoodPoints = 100;
		SoundManager.instance.musicSource.Play ();
		Application.LoadLevel ("Game");
	}

	void OnApplicationQuit ()
	{
		Instance = null;
	}

}
