﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour
{
	public GameManager GameManager;
	
	void Awake ()
	{
		if (GameManager.Instance == null) {
			Instantiate (GameManager);
		}
		
		GameManager.Instance.OnStateChange += OnStateChange;
	}

	void Start ()
	{
		GameManager.Instance.SetGameState (GameState.INTRO);
	}

	void OnStateChange ()
	{
		GameManager.Instance.OnStateChange -= OnStateChange;
		Debug.Log ("Handling state change to: " + GameManager.Instance.gameState);
		Invoke ("LoadLevel", 3f);
	}

	void LoadLevel ()
	{
		Application.LoadLevel ("MainMenu");
	}
}
